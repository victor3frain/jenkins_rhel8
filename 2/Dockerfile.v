###########################################
# Stage 1: Build go-init
###########################################

#FROM openshift/origin-release:golang-1.12 
FROM registry.redhat.io/rhel8/go-toolset:1.16.12-7.1647529280
USER root
RUN cat /etc/*release
RUN subscription-manager list
RUN subscription-manager clean
RUN subscription-manager list
RUN subscription-manager register --username=hector.jrm --password=AllenWalker3087 --auto-attach
RUN  subscription-manager list

RUN yum update -y
RUN yum upgrade -y 
#RUN yum install go-toolset-1.11 -y


MAINTAINER OpenShift Developer Services <openshift-dev-services+jenkins@redhat.com>

ENV JENKINS_VERSION=2 \
    HOME=/var/lib/jenkins \
    JENKINS_HOME=/var/lib/jenkins \
    JENKINS_UC=https://updates.jenkins.io \
    OPENSHIFT_JENKINS_IMAGE_VERSION=4.10 \
    LANG=en_US.UTF-8 \
    LC_ALL=en_US.UTF-8 \
    INSTALL_JENKINS_VIA_RPMS=false
# openshift/ocp-build-data will change INSTALL_JENKINS_VIA_RPMS to true
# so that the osbs/brew builds will install via RPMs; when this runs 
# in api.ci, it will employ the old centos style, download the plugins and
# redhat-stable core RPM for download

LABEL io.k8s.description="Jenkins is a continuous integration server" \
      io.k8s.display-name="Jenkins 2" \
      io.openshift.tags="jenkins,jenkins2,ci" \
      io.openshift.expose-services="8080:http" \
      io.jenkins.version="${jenkins_version}" \
      io.openshift.s2i.scripts-url=image:///usr/libexec/s2i 

# Labels consumed by Red Hat build service
LABEL com.redhat.component="openshift-jenkins-2-container" \
      name="openshift4/jenkins-2-rhel7" \
      architecture="x86_64 \
      maintainer="openshift-dev-services+jenkins@redhat.com""

# 8080 for main web interface, 50000 for slave agents
EXPOSE 8080 50000

RUN ln -s /usr/lib/jenkins /usr/lib64/jenkins && \
    INSTALL_PKGS="dejavu-sans-fonts wget rsync gettext git tar zip unzip openssl bzip2 java-11-openjdk java-11-openjdk-devel java-1.8.0-openjdk java-1.8.0-openjdk-devel jq glibc-locale-source" && \
#    INSTALL_PKGS="dejavu-sans-fonts wget rsync gettext git tar zip unzip openssl bzip2 java-11-openjdk java-11-openjdk-devel java-1.8.0-openjdk java-1.8.0-openjdk-devel jq glibc-locale-source xmlstarlet" && \
    yum install -y $INSTALL_PKGS && \
    rpm -V  $INSTALL_PKGS && \
    yum clean all

COPY ./contrib/openshift /opt/openshift
COPY ./contrib/jenkins /usr/local/bin
ADD ./contrib/s2i /usr/libexec/s2i
ADD release.version /tmp/release.version

RUN /usr/local/bin/install-jenkins-core-plugins.sh /opt/openshift/bundle-plugins.txt && \
    rmdir /var/log/jenkins && \
    chmod -R 775 /etc/alternatives && \
    chmod -R 775 /var/lib/alternatives && \
    chmod -R 775 /usr/lib/jvm && \
    chmod 775 /usr/bin && \
    chmod 775 /usr/share/man/man1 && \
    mkdir -p /var/lib/origin && \
    chmod 775 /var/lib/origin && \
    unlink /usr/bin/java && \
    unlink /usr/bin/jjs && \
    unlink /usr/bin/keytool && \
    unlink /usr/bin/pack200 && \
    unlink /usr/bin/rmid && \
    unlink /usr/bin/rmiregistry && \
    unlink /usr/bin/unpack200 && \
    unlink /usr/share/man/man1/java.1.gz && \
    unlink /usr/share/man/man1/jjs.1.gz && \
    unlink /usr/share/man/man1/keytool.1.gz && \
    unlink /usr/share/man/man1/pack200.1.gz && \
    unlink /usr/share/man/man1/rmid.1.gz && \
    unlink /usr/share/man/man1/rmiregistry.1.gz && \
    unlink /usr/share/man/man1/unpack200.1.gz && \
    chown -R 1001:0 /opt/openshift && \
    /usr/local/bin/fix-permissions /opt/openshift && \
    /usr/local/bin/fix-permissions /opt/openshift/configuration/init.groovy.d && \
    /usr/local/bin/fix-permissions /var/lib/jenkins && \
    /usr/local/bin/fix-permissions /var/log

ARG jenkins_version=latest
WORKDIR  /go/src/github.com/openshift/jenkins
COPY . .
RUN go version
WORKDIR  /go/src/github.com/openshift/jenkins/go-init
RUN GO111MODULE=off go build . && cp go-init /usr/bin
RUN echo "victor3frain"
RUN cat /etc/*release

COPY ./ /go/src/go-init 
#RUN go install go-init
#RUN ls -l /go/bin/go-init
#RUN go version
#RUN jenkins --version

RUN yum update jenkins 
VOLUME ["/var/lib/jenkins"]
RUN cat /etc/*release
USER 1001
ENTRYPOINT ["/usr/bin/go-init", "-main", "/usr/libexec/s2i/run"]
#ENTRYPOINT ["yum update jenkins "]


###########################################
# Stage 2: ubi-minimal image with go-init
###########################################

#FROM registry.access.redhat.com/ubi7/ubi-minimal:latest
#COPY --from=builder /go/bin/go-init /usr/bin/go-init
#COPY --from=builder /go/bin/* /usr/bin/
#RUN ls -l /usr/bin
#RUN go version
